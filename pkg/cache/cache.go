package cache

import (
	"crypto/md5"
	"fmt"
	"time"
	"zong-go/pkg/config"
	"zong-go/pkg/logger"
	"zong-go/pkg/redis"
)

// RedisKey
func RedisKey(key string) string {
	appKey := key + config.Get("app", "key")
	md5Byte := md5.Sum([]byte(appKey))
	md5String := fmt.Sprintf("%x", md5Byte)

	return md5String
}

// exists 检测缓存项是否存在
func Exists(keys string) bool {
	if result, err := redis.Redis.Exists(RedisKey(keys)).Result(); err != nil {
		logger.LogError(err)
		return false
	} else {
		if result > 0 {
			return true
		} else {
			return false
		}
	}
}

// Set 设置缓存项,过期时间（秒）
func Set(key string, value interface{}, expiration int64) {
	if _, err := redis.Redis.Set(RedisKey(key), value, time.Duration(expiration)*time.Second).Result(); err != nil {
		logger.LogError(err)
	}
}

// Get 获取缓存项
func Get(key string) interface{} {
	if result, err := redis.Redis.Get(RedisKey(key)).Result(); err != nil {
		logger.LogError(err)
		return nil
	} else {
		return result
	}
}

// Del 删除缓存项
func Del(key string) bool {
	if result, err := redis.Redis.Del(RedisKey(key)).Result(); err != nil {
		logger.LogError(err)
		return false
	} else {
		if result > 0 {
			return true
		} else {
			return false
		}
	}
}

// FlushDB 清空当前数据库
func FlushDB() {
	redis.Redis.FlushDB()
}

// FlushAll 清空所有数据库
//func FlushAll() {
//	redis.Redis.FlushAll()
//}
