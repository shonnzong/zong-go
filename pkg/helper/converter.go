package helper

import (
	"encoding/json"
	"strconv"
	"zong-go/pkg/logger"
)

// AnyToString 获取变量的字符串值
// 浮点型 3.0将会转换成字符串3, "3"
// 非数值或字符类型的变量将会被转换成JSON格式字符串
func AnyToString(value interface{}) string {
	// interface 转 string
	var key string
	if value == nil {
		return key
	}

	switch value.(type) {
	case float64:
		ft := value.(float64)
		key = strconv.FormatFloat(ft, 'f', -1, 64)
	case float32:
		ft := value.(float32)
		key = strconv.FormatFloat(float64(ft), 'f', -1, 64)
	case int:
		it := value.(int)
		key = strconv.Itoa(it)
	case uint:
		it := value.(uint)
		key = strconv.Itoa(int(it))
	case int8:
		it := value.(int8)
		key = strconv.Itoa(int(it))
	case uint8:
		it := value.(uint8)
		key = strconv.Itoa(int(it))
	case int16:
		it := value.(int16)
		key = strconv.Itoa(int(it))
	case uint16:
		it := value.(uint16)
		key = strconv.Itoa(int(it))
	case int32:
		it := value.(int32)
		key = strconv.Itoa(int(it))
	case uint32:
		it := value.(uint32)
		key = strconv.Itoa(int(it))
	case int64:
		it := value.(int64)
		key = strconv.FormatInt(it, 10)
	case uint64:
		it := value.(uint64)
		key = strconv.FormatUint(it, 10)
	case string:
		key = value.(string)
	case []byte:
		key = string(value.([]byte))
	default:
		newValue, _ := json.Marshal(value)
		key = string(newValue)
	}

	return key
}

// StringToInt 将字符串转换为 int
func StringToInt(str string) int {
	i, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}
	return i
}

// InterfaceToJson 将 interface 转换为 json 字符串
func InterfaceToJson(i interface{}) string {
	jsons, err := json.Marshal(i)
	if err != nil {
		return ""
	}
	return string(jsons)
}

// JsonToInterface 将 json 字符串转换为 interface
func JsonToInterface(s []byte) interface{} {
	var result interface{}
	err := json.Unmarshal(s, &result)
	if err != nil {
		return nil
	}
	return result
}

// JsonToMapString 将 json 字符串转换为 map
func JsonToMapString(s []byte) map[string]interface{} {
	var result map[string]interface{}
	err := json.Unmarshal(s, &result)
	if err != nil {
		return nil
	}
	return result
}

// JsonToSliceMapString 将 json 字符串转换为 []map
func JsonToSliceMapString(s []byte) []map[string]interface{} {
	var result []map[string]interface{}
	err := json.Unmarshal(s, &result)
	if err != nil {
		return nil
	}
	return result
}

// StructToJson 将 struct 转换为 json 字符串
func StructToJson(i interface{}) string {
	jsons, err := json.Marshal(i)
	if err != nil {
		return ""
	}
	return string(jsons)
}

// JsonToStruct 将 json 字符串转换为 struct
func JsonToStruct(s []byte, ss interface{}) {
	err := json.Unmarshal(s, ss)
	logger.LogError(err)
}
