package helper

import (
	"sort"
	"zong-go/app/handlers/structs"
)

type SortUser structs.UserData // 监控列表排序

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func GetSortUser(s structs.UserData) structs.UserData {
	sort.Sort(SortUser(s))
	return s
}
func (s SortUser) Len() int { // 重写 Len() 方法
	return len(s)
}
func (s SortUser) Swap(i, j int) { // 重写 Swap() 方法
	s[i], s[j] = s[j], s[i]
}
func (s SortUser) Less(i, j int) bool { // 重写 Less() 方法， 从大到小排序
	return s[j].ID < s[i].ID
}
