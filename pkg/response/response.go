package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// Response 200
func ResponseSuccess(context *gin.Context, message string, data interface{}) {
	context.JSON(http.StatusOK, gin.H{
		"code":    0,
		"message": message,
		"data":    data,
	})
}

// Response 200 Error
func ResponseError(context *gin.Context, message string) {
	context.JSON(http.StatusOK, gin.H{
		"code":    1,
		"message": message,
		"data":    map[string]interface{}{},
	})
}

// Response 400
func ResponseBadRequest(context *gin.Context) {
	context.JSON(http.StatusBadRequest, gin.H{
		"code":    10400,
		"message": "Bad Request.",
	})
}

// Response 401
func ResponseUnauthorized(context *gin.Context, message string) {
	context.JSON(http.StatusUnauthorized, gin.H{
		"code":    10401,
		"message": message,
	})
}

// Response 404
func ResponseNotFound(context *gin.Context) {
	context.JSON(http.StatusNotFound, gin.H{
		"code":    10404,
		"message": "Not Found.",
	})
}

// Response 405
func ResponseMethodNotAllowed(context *gin.Context) {
	context.JSON(http.StatusMethodNotAllowed, gin.H{
		"code":    10405,
		"message": "Method not allowed.",
	})
}

// Response 500
func ResponseServerError(context *gin.Context, message string) {
	context.JSON(http.StatusInternalServerError, gin.H{
		"code":    10500,
		"message": message,
	})
}
