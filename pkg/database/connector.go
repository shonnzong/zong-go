package database

import "gorm.io/gorm"

// DB gorm.DB 对象
var DB *gorm.DB

// ConnData 数据库配置
type ConnData struct {
	Connection string
	Host       string
	Port       string
	Database   string
	Username   string
	Password   string
	Charset    string
}
