package scheduler

type QueueScheduler struct {
	requestChan chan Request
	workerChan  chan chan Request
}

func (s *QueueScheduler) WorkerChan() chan Request {
	return make(chan Request)
}

func (s *QueueScheduler) Submit(r Request) {
	s.requestChan <- r
}

func (s *QueueScheduler) WorkerReady(w chan Request) {
	s.workerChan <- w
}

func (s *QueueScheduler) Run() {
	s.workerChan = make(chan chan Request)
	s.requestChan = make(chan Request)
	go func() {
		var requestQueue []Request
		var workerQueue []chan Request
		for {
			var activeRequest Request
			var activeWorker chan Request
			if len(requestQueue) > 0 && len(workerQueue) > 0 {
				activeRequest = requestQueue[0]
				activeWorker = workerQueue[0]
			}
			select {
			case r := <-s.requestChan:
				requestQueue = append(requestQueue, r)
			case w := <-s.workerChan:
				workerQueue = append(workerQueue, w)
			case activeWorker <- activeRequest:
				requestQueue = requestQueue[1:]
				workerQueue = workerQueue[1:]
			}
		}
	}()
}
