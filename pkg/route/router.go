package route

import (
	"zong-go/routes"
	"github.com/gin-gonic/gin"
)

func RegisterRouter(engine *gin.Engine) {
	// 注册WEB路由
	routes.RegisterWebRoutes(engine)
	// 注册API路由
	routes.RegisterApiRoutes(engine)
}
