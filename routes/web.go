package routes

import (
	"github.com/didip/tollbooth"
	"github.com/gin-gonic/gin"
	webController "zong-go/app/http/controllers/web"
	"zong-go/app/http/middlewares"
)

func RegisterWebRoutes(engine *gin.Engine) {
	// HTML 渲染
	engine.LoadHTMLGlob("resources/view/*")
	//engine.LoadHTMLFiles("resources/view/index.html")
	// 设置静态资源
	engine.Static("/static", "resources/static")
	// API路由设置
	web := engine.Group("/")
	// 限流、跨域访问 全局设置
	web.Use(middlewares.LimiterMiddleware(tollbooth.NewLimiter(100, nil))) // 100 请求/秒限制器.
	web.Use(middlewares.CorsMiddleware())
	// 不需要经过JWT中间件验证就能访问的接口
	//web.GET("", webController.IndexController{}.Index)
	// JWT中间件 全局设置
	web.Use(middlewares.JwtMiddleware())

	// Index
	web.GET("", webController.IndexController{}.Index)
}
