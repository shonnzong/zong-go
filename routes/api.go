package routes

import (
	"github.com/didip/tollbooth"
	"github.com/gin-gonic/gin"
	apiController "zong-go/app/http/controllers/api"
	"zong-go/app/http/middlewares"
)

func RegisterApiRoutes(engine *gin.Engine) {
	// API路由设置
	api := engine.Group("/api")
	// 限流、跨域访问、JWT中间件 全局设置
	api.Use(middlewares.LimiterMiddleware(tollbooth.NewLimiter(100, nil))) // 100 请求/秒限制器.
	api.Use(middlewares.CorsMiddleware())
	// 不需要经过JWT中间件验证就能访问的接口
	api.POST("/auth", apiController.AuthController{}.Login)
	api.GET("/ws", apiController.WsController{}.Ws)
	// JWT中间件 全局设置
	api.Use(middlewares.JwtMiddleware())

	// JWT Bearer Token验证
	authGroup := api.Group("/auth")
	authGroup.DELETE("", apiController.AuthController{}.Logout)
	authGroup.PUT("", apiController.AuthController{}.Refresh)
	// 用户路由
	userGroup := api.Group("/user")
	userGroup.POST("", apiController.UserController{}.Create)
	userGroup.PATCH("", apiController.UserController{}.Update)
	userGroup.DELETE("", apiController.UserController{}.Delete)
	userGroup.GET("", apiController.UserController{}.Index)
	userGroup.GET("/me", apiController.UserController{}.Me)
	userGroup.PATCH("/reset", apiController.UserController{}.UpdatePassword)

}
