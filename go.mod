module zong-go

go 1.16

require (
	github.com/Unknwon/goconfig v0.0.0-20200908083735-df7de6a44db8
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/didip/tollbooth v4.0.2+incompatible
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/gin-gonic/gin v1.7.2
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/sessions v1.2.1
	github.com/gorilla/websocket v1.4.2
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	gopkg.in/boj/redistore.v1 v1.0.0-20160128113310-fc113767cd6b
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.12
)
