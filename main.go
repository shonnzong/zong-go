package main

import (
	"github.com/gin-gonic/gin"
	"zong-go/config"
	c "zong-go/pkg/config"
	"zong-go/pkg/db"
	"zong-go/pkg/logger"
	"zong-go/pkg/redis"
	"zong-go/pkg/route"
)

func init() {
	// 初始化配置文件
	config.Initialize()
}

func main() {
	// 加载Gin框架
	app := gin.Default()
	// 注册路由
	route.RegisterRouter(app)
	// 初始化 SQL
	db.InitDB()
	// 初始化 Redis
	redis.InitRedis()
	// 初始化 websocket 服务
	//go ws.Manager.Start()
	// 启动服务
	err := app.Run(c.Get("app", "url") + ":" + c.Get("app", "port"))
	logger.LogError(err)
}
