:: 转换 UTF-8 编码
chcp 65001
:: 注册 MySQL 服务
mysqld --install
:: 注册 Redis 服务,下载地址: https://github.com/tporadowski/redis/releases
redis-server --service-install redis.windows.conf