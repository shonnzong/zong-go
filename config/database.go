package config

import (
	"zong-go/pkg/config"
)

func init() {
	// 数据库连接信息
	config.Set("database", "connection", config.Env("", "DB_CONNECTION", "mysql"))
	config.Set("database", "host", config.Env("", "DB_HOST", "127.0.0.1"))
	config.Set("database", "port", config.Env("", "DB_PORT", "3306"))
	config.Set("database", "database", config.Env("", "DB_DATABASE", "database"))
	config.Set("database", "username", config.Env("", "DB_USERNAME", ""))
	config.Set("database", "password", config.Env("", "DB_PASSWORD", ""))
	config.Set("database", "charset", config.Env("", "DB_CHARSET", "utf8mb4"))
	// 连接池配置
	config.Set("database", "max_idle_connections", config.Env("", "DB_MAX_IDLE_CONNECTIONS", "100"))
	config.Set("database", "max_open_connections", config.Env("", "DB_MAX_OPEN_CONNECTIONS", "25"))
	config.Set("database", "max_life_seconds", config.Env("", "DB_MAX_LIFE_SECONDS", "300"))
}
