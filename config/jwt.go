package config

import "zong-go/pkg/config"

func init() {
	// jwt加密字符串
	config.Set("jwt", "secret", config.Env("", "JWT_SECRET", "RJeiEVlz0xHIkmohMrmcW82TVJloT8WLLaduz1u8FelouvHeLIFEydpac1czRnY3"))
	// 过期时间,分
	config.Set("jwt", "expires", config.Env("", "JWT_EXPIRES", "60"))
}
