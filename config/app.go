package config

import (
	"zong-go/pkg/config"
)

func init() {
	// 应用名称，暂时没有使用到
	config.Set("app", "name", config.Env("", "APP_NAME", "ShonnZong"))
	// 当前环境，用以区分多环境
	config.Set("app", "env", config.Env("", "APP_ENV", "production"))
	// 是否进入调试模式
	config.Set("app", "debug", config.Env("", "APP_DEBUG", "false"))
	// 应用服务地址
	config.Set("app", "url", config.Env("", "APP_URL", "localhost"))
	// 应用服务端口
	config.Set("app", "port", config.Env("", "APP_PORT", "3000"))
	// 应用服务加密字符串
	config.Set("app", "key", config.Env("", "APP_KEY", "33446a9dcf9ea060a0a6532b166da32f304af0de"))
}
