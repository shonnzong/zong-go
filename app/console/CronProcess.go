package console

import (
	"time"
	"zong-go/pkg/cron"
)

var wsTimer *time.Timer

// sendWs 发送广播信息
func sendWs() {
	p := cron.CronProcess{
		ProcessTimer: wsTimer,
		ProcessFunc: []func() string{
			// Some func to do
		},
		ProcessPeriod: time.Second * 5, // 间隔5秒一次
	}
	p.Start()
}
