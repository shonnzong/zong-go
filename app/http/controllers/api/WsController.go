package controller

import (
	"zong-go/pkg/ws"
	"github.com/gin-gonic/gin"
)

type WsController struct {
	wss ws.WsServer
}

// Ws 连接 websocket
func (wsController *WsController) Ws(context *gin.Context) {
	wsController.wss.Server(context)
}
