package controller

import "github.com/gin-gonic/gin"

type IndexController struct {
}

func (index IndexController) Index(context *gin.Context) {
	context.HTML(200, "index.html", gin.H{})
}
