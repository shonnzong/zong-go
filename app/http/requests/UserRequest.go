package request

type UserCreateRequest struct {
	Username string `json:"username" binding:"required,max=50"`
	Name     string `json:"name" binding:"required,max=50"`
	Phone    string `json:"phone" binding:"required,max=11"`
	Password string `json:"password" binding:"required,max=100"`
	Sex      *int   `json:"sex"`
	Status   *int   `json:"status"`
}

type UserUpdateRequest struct {
	ID     uint64 `json:"id" binding:"required"`
	Name   string `json:"name" binding:"required,max=50"`
	Phone  string `json:"phone" binding:"required,max=11"`
	Sex    *int   `json:"sex"`
	Status *int   `json:"status"`
}

type UserDeleteRequest struct {
	ID uint64 `json:"id" binding:"required"`
}

type UserIndexRequest struct {
	ID       uint64 `form:"id"`
	Username string `form:"username" binding:"max=50"`
}

type UserPasswordRequest struct {
	Username        string `json:"username" binding:"required,max=50"`
	Password        string `json:"password" binding:"required,max=100"`
	PasswordComfirm string `json:"password_comfirm" binding:"required,max=100"`
}
